import os
import re
import falcon
import threading
import urllib.parse
import RepositoryMetadataManager

# Falcon Resource handler for Gitlab System Hook events
class CommitRedirector(object):

	# Initial Setup
	def __init__(self, configuration, repositoryMetadataPath):
		# Store everything we need to operate
		self.configuration = configuration
		self.metadataPath  = repositoryMetadataPath
		self.canUpdate     = True

		# Deduce the root of the metadata repository
		self.metadataRoot  = os.path.join( self.metadataPath.rstrip("/"), '..' )
		self.metadataRoot  = os.path.abspath(self.metadataRoot)

		# Initialize ourselves
		self.metadataRepoMTime = 0
		self._updateMetadata()

	# Function to assist with mapping a given path back to a repository
	def _resolve_repository(self, path):
		# First we check to see if we have a / in the provided path
		# If we do, then we definitely have a path to lookup so take a look there
		if '/' in path and path in self.repositoryManager.knownPaths:
			# Retrieve and return the repository information
			return self.repositoryManager.knownPaths[ path ]

		# If we don't have a slash, then we probably have a repository identifier...
		if path in self.repositoryManager.knownIdentifiers:
			# In which case we should provide the appropriate repository information too
			return self.repositoryManager.knownIdentifiers[ path ]

		# Otherwise we don't know anything about it
		return None

	# Function to assist with updating the metadata
	def _updateMetadata(self):
		# Determine the last modification time of the metadata we are using
		# For this we rely on HEAD in the underlying Git repository
		gitHeadPath = os.path.join( self.metadataRoot, ".git", "HEAD" )
		newMTime = os.stat( gitHeadPath ).st_mtime

		# Has the metadata been updated?
		if newMTime > self.metadataRepoMTime:
            # Save the new change time for later checks...
			self.metadataRepoMTime = newMTime
			# Rebuild the repository metadata manager
			self.repositoryManager = RepositoryMetadataManager.RepositoryMetadataManager(self.metadataPath)

	# Function to be called when cooldown timer is complete
	def _coolDown(self):
		self.canUpdate = True

	# Receive a query from a user
	#
	# These can take several forms:
	# - https://commits.kde.org/repo-management/33df71f2414b0f0c4dcc0e1ce84399dabbe00663
	# - https://commits.kde.org/repo-management?branch=master
	# - https://commits.kde.org/repo-management?tag=3.0
	# - https://commits.kde.org/repo-management?path=README.txt
	# - https://commits.kde.org/repo-management?path=README.txt&branch=master
	#
	# With the exception of the first one, all of the parameters specified can be used together (although branch/tag together wouldn't really make sense)
	#
	def on_get(self, req, resp, path):
		# Do we need to update the metadata we are working with?
		if self.canUpdate:
			# Trigger the update process
			self._updateMetadata()
			# Start the cooldown process
			self.canUpdate = False
			threading.Timer(5.0, self._coolDown).start()

		# Determine where our Gitlab instance lives
		gitlabInstance = self.configuration.get('Gitlab', 'instance')

		# Make sure we got given something we can work with....
		# If there is no path, then we just send them over to Gitlab
		if path == "":
			# Make it so!
			raise falcon.HTTPMovedPermanently( gitlabInstance )

		# If the path given has a trailing slash then we should remove it
		path = path.rstrip('/')
		# Likewise if it has a trailing .git
		if path.endswith('.git'):
			path = path.rstrip('.git')

		# First, we check to see if we have a commit hash in our url...
		commitMatch = re.match("(.+?)(.git)?\/([a-f0-9]{8,40})", path)
		if commitMatch is not None:
			# Extract the necessary information...
			repoPath   = commitMatch.group(1)
			commitHash = commitMatch.group(3)

			# Resolve the path of the repository on Gitlab
			repository = self._resolve_repository( repoPath )

			# Make sure we found a repository
			# If we have nothing then we cannot help the user here
			if repository is None:
				raise falcon.HTTPNotFound()

			# Construct the URL to redirect to
			url = "{instance}/{repository}/-/commit/{commit}".format( instance=gitlabInstance, repository=repository['repopath'], commit=commitHash )

			# Send the user away!
			raise falcon.HTTPMovedPermanently( url )

		# Secondly, could this be a request from a Git client?
		gitClientMatch = re.match("(.+?)(.git)?/info/.*", path)
		if gitClientMatch is not None:
			# Extract the information
			repoPath = gitClientMatch.group(1)

			# Resolve the path of the repository on Gitlab
			repository = self._resolve_repository( repoPath )

			# Make sure we found a repository
			# If we have nothing then we cannot help the user here
			if repository is None:
				raise falcon.HTTPNotFound()

			# Construct the URL to redirect to
			url = "{instance}/{repository}.git/info/refs?service=git-upload-pack".format( instance=gitlabInstance, repository=repository['repopath'] )

			# Send the user away!
			raise falcon.HTTPMovedPermanently( url )

		# Otherwise we assume we have a general repository url
		# First try to resolve the repository
		repository = self._resolve_repository( path )

		# Make sure we found a repository
		# If we have nothing then we cannot help the user here
		if repository is None:
			raise falcon.HTTPNotFound()

		# Now that we have a repository, we can get started building our url!
		# Lets begin by using a basic url that just links to the repository...
		url      = "{instance}/{repository}"
		gitRef   = "master"
		filePath = ""

		# Do we have a branch, tag or file path?
		if 'branch' in req.params or 'tag' in req.params or 'path' in req.params:
			# Then we need to change the url template
			url = "{instance}/{repository}/-/tree/{ref}/{path}"

		# Did we get a branch?
		if 'branch' in req.params:
			# Then update the Git reference too
			gitRef = urllib.parse.quote( req.params['branch'] )

		# Otherwise did we get a tag?
		if 'tag' in req.params:
			# Then we need to change the Git reference in this case too
			gitRef = urllib.parse.quote( req.params['tag'] )

		# Maybe we got a path?
		if 'path' in req.params:
			# Make sure we make use of it then
			filePath = urllib.parse.quote( req.params['path'] )

		# Build the URL now that we know what we need to do!
		url = url.format( instance=gitlabInstance, repository=repository['repopath'], ref=gitRef, path=filePath )

		# Serve the redirect!
		raise falcon.HTTPMovedPermanently( url )

