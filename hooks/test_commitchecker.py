#!/usr/bin/python

# SPDX-FileCopyrightText: 2020 Andreas Cord-Landwehr <cordlandwehr@kde.org>
# SPDX-License-Identifier: BSD-3-Clause

from hooklib import CommitChecker

checker = CommitChecker()

import glob

testfiles = glob.glob("testdata/licensetest/*")
for testfile in testfiles:
    file = open(testfile, "r")
    content = file.read()
    problemfile, license = checker.check_commit_license_spdx(testfile, content)
    if not problemfile:
        print("OK    " + license + " " + testfile)
    else:
        print("ERROR " + license + " " + testfile)
